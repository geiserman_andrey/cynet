﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleReader
{
    public static class FileHandler
    {
        public static string CreateNewRulesFile(string path, string rule, string fileName)
        {
            try
            {
                using (StreamWriter sw = File.CreateText(path + @"\" + fileName))
                {
                    sw.WriteLine(rule);
                    return path + @"\" + fileName;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public static string CreateNewHostsFile(string path, string hostPacket, string fileName)
        {
            try
            {
                using (StreamWriter sw = File.CreateText(path + @"\" + fileName))
                {
                    sw.WriteLine(hostPacket);
                    // should return path to files not a specific file
                    return path;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
