﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleReader
{
    public struct Rule
    {
        private string User { get; set; }
        private string Destination { get; set; }
        private string RuleAction { get; set; }

        public Rule(string user = "TestUser@domain.com",
                    string destination = "192.168.0.1",
                    string ruleAction = "deny")
        {
            User = user;
            Destination = destination;
            RuleAction = ruleAction;
        }

        public string GetRule()
        {
            return string.Format("<{0}>|<{1}>|<{2}>", User, Destination, RuleAction);
        }
    }
}
