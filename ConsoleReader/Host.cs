﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleReader
{
    public class Host
    {
        private string User { get; set; }
        private string Destination { get; set; }

        public Host(string user = "TestUser@domain.com", string destination = "192.168.0.1")
        {
            User = user;
            Destination = destination;
        }

        public string GetHostPacket()
        {
            return string.Format("<{0}>|<{1}>", User, Destination);
        }
    }
}
