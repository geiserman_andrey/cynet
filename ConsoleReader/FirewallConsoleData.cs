﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace ConsoleReader
{
    public class FirewallConsoleData
    {
        Process _process;

        public FirewallConsoleData(string arguments)
        {
            _process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = ConfigurationManager.AppSettings["FirewallPath"],
                    Arguments = arguments,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            _process.Start();
        }

        public int ReturnExitCode()
        {
            while (!_process.StandardOutput.EndOfStream)
            {
                string line = _process.StandardOutput.ReadLine();
                Console.WriteLine(line);
            }
            _process.WaitForExit();
            return _process.ExitCode;
        }

        void WriteConsoleOutputToFile(string file, string stream)
        {
            using (StreamWriter writetext = File.AppendText(file))
            {
                writetext.WriteLine(stream);
            }
        }

        public List<Response> GetConsoleOutput()
        {
            List<Response> responseList = new List<Response>();

            while (!_process.StandardOutput.EndOfStream)
            {
                string line = _process.StandardOutput.ReadLine();
                responseList.Add(ParseResponse(line));
            }
            _process.WaitForExit();
            return responseList;
        }

        public Response ParseResponse(string obj)
        {
            string[] subStrings = obj.Split(' ');
            Response tmpResponse = new Response(subStrings[0], subStrings[2], subStrings[4], subStrings[6]);
            return tmpResponse;
        }
    }
}
