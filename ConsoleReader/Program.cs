﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleReader
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start of program");
            Console.WriteLine(ConfigurationManager.AppSettings["FirewallPath"]);
            Console.WriteLine("End of program");
           
            Console.ReadKey();
        }
    }
}
