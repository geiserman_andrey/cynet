﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleReader
{
    public struct Response
    {
        public string Host { get; set; }
        public string Destination { get; set; }
        public string User { get; set; }
        public string Action { get; set; }

        public Response(string host, string destination, string user, string action)
        {
            Host = host;
            Destination = destination;
            User = user;
            Action = action;
        }
    }
}
