﻿using ConsoleReader;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    [TestFixture]
    public class FirewallTestsForHostFile
    {
        private static DirectoryInfo _ruleFileDir = null;
        private static DirectoryInfo _hostsFilesDir = null;

        private FirewallConsoleData _console;

        [SetUp]
        public void TestFixtureSetUp()
        {
            // Set current folder to testing folder
            string assemblyCodeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            string dirName = Path.GetDirectoryName(assemblyCodeBase);

            if (dirName.StartsWith("file:\\"))
                dirName = dirName.Substring(6);
            Environment.CurrentDirectory = dirName;

            _ruleFileDir = System.IO.Directory.CreateDirectory(dirName + ConfigurationManager.AppSettings["DirNameForRulesFile"]);
            _hostsFilesDir = System.IO.Directory.CreateDirectory(dirName + ConfigurationManager.AppSettings["DirNameForHostsFile"]);
        }

        [TearDown]
        public void TestMixtureTearDown()
        {
            foreach (FileInfo file in _ruleFileDir.GetFiles())
            {
                file.Delete();
            }
            foreach (FileInfo file in _hostsFilesDir.GetFiles())
            {
                file.Delete();
            }
        }

        [Test]
        // Invalid User in hosts file
        [TestCase("HostFileUserdomain.com", "192.168.1.1", 2)]
        [TestCase("HostFileUser@domaincom", "192.168.1.1", 2)]
        [TestCase("HostFileUser@domain.co.", "192.168.1.1", 2)]
        [TestCase("HostFileUser@domain.co.i", "192.168.1.1", 2)]
        [TestCase("domain.com", "192.168.1.1", 2)]
        // Invalid Destination in hosts file
        [TestCase("HostFileUser@domain.com", "192.168.1.", 2)]
        [TestCase("HostFileUser@domain.com", "192.168.1", 2)]
        [TestCase("HostFileUser@domain.com", "192.168..1", 2)]
        [TestCase("HostFileUser@domain.com", "192..1.1", 2)]
        [TestCase("HostFileUser@domain.com", ".168.1.1", 2)]
        [TestCase("HostFileUser@domain.com", "192. . . ", 2)]
        [TestCase("HostFileUser@domain.com", "...", 2)]
        public void FirewallTests_WhenInvalidHostsFilesProvided_ReturnCorrespondingExitCode(string user, string destination, int expectedExitCode)
        {
            string rule = new Rule().GetRule();
            string rulesFileName = "Rules_" + DateTime.UtcNow.Ticks + ".csv";
            string hostPacket = new Host(user, destination).GetHostPacket();
            string hostFileName = "192.168.1.10.csv";

            var newRulesFilePath = FileHandler.CreateNewRulesFile(_ruleFileDir.FullName, rule, rulesFileName);
            var newHostFilePath = FileHandler.CreateNewHostsFile(_hostsFilesDir.FullName, hostPacket, hostFileName);

            _console = new FirewallConsoleData(newRulesFilePath + " " + newHostFilePath);

            Assert.AreEqual(expectedExitCode, _console.ReturnExitCode());
        }
    }
}
