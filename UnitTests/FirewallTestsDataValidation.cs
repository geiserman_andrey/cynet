﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleReader;
using System.Configuration;

namespace UnitTests
{
    [TestFixture]
    public class FirewallTestsDataValidation
    {
        private static DirectoryInfo _ruleFileDir = null;
        private static DirectoryInfo _hostsFilesDir = null;
        private FirewallConsoleData _console;

        [SetUp]
        public void TestFixtureSetUp()
        {
            // Set current folder to testing folder
            string assemblyCodeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            string dirName = Path.GetDirectoryName(assemblyCodeBase);

            if (dirName.StartsWith("file:\\"))
                dirName = dirName.Substring(6);
            Environment.CurrentDirectory = dirName;

            _ruleFileDir = System.IO.Directory.CreateDirectory(dirName+ ConfigurationManager.AppSettings["DirNameForRulesFile"]);
            _hostsFilesDir = System.IO.Directory.CreateDirectory(dirName + ConfigurationManager.AppSettings["DirNameForHostsFile"]);
        }

        [TearDown]
        public void TestMixtureTearDown()
        {
            foreach (FileInfo file in _ruleFileDir.GetFiles())
            {
                file.Delete();
            }
            foreach (FileInfo file in _hostsFilesDir.GetFiles())
            {
                file.Delete();
            }
        }

        /// <summary>
        /// This test verifies action provided in rules file
        /// If action in rules file set to "allow", result should be "allowed"
        /// If action in rules file set to "deny", result should be "denied"
        /// </summary>
        /// <param name="user"></param>
        /// <param name="destination"></param>
        /// <param name="action"></param>
        /// <param name="expectedResult"></param>
        [Test]
        [TestCase("OiInternet@domain.com", "177.41.11.124", "allow", "allowed")]
        [TestCase("OiInternet@domain.com", "177.41.11.124", "deny", "denied")]
        public void FirewallTests_TestRuleActionField_ReturnsCorrespondingAction(string user, string destination, string action, string expectedResult)
        {
            string rule = new Rule(user, destination, action).GetRule();
            string rulesFileName = "Rules_Deny_" + DateTime.UtcNow.Ticks + ".csv";
            string hostPacket = new Host(user, destination).GetHostPacket();
            string hostFileName = "192.168.1.10.csv";

            var newRulesFilePath = FileHandler.CreateNewRulesFile(_ruleFileDir.FullName, rule, rulesFileName);
            var newHostFilePath = FileHandler.CreateNewHostsFile(_hostsFilesDir.FullName, hostPacket, hostFileName);

            _console = new FirewallConsoleData(newRulesFilePath + " " + newHostFilePath);

            Assert.That(_console.GetConsoleOutput()[0].Action, Is.EqualTo(expectedResult));
        }

        /// <summary>
        /// FirewallTests_ReloadDataForRuleFile_ReturnedNewData
        /// Change value in rule file and see the change reflected in Firewall's output
        /// </summary>
        /// <param name="user"></param>
        /// <param name="destination"></param>
        /// <param name="ruleAction"></param>
        /// <param name="expectedResult"></param>
        [Test]
        [TestCase("TestUser@domain.com", "192.168.0.1", "allow", "denied")]
        public void FirewallTests_ReloadDataForRuleFile_ReturnedNewData(string user, string destination,  string ruleAction,  string expectedResult)
        {
            Rule rule = new Rule(user, destination, ruleAction);
            string rulesFileName = "Rules_Reload_.csv";
            string hostPacket = new Host().GetHostPacket();
            string hostFileName = "192.168.1.10.csv";
            var newRulesFilePath = FileHandler.CreateNewRulesFile(_ruleFileDir.FullName, rule.GetRule(), rulesFileName);
            var newHostFilePath = FileHandler.CreateNewHostsFile(_hostsFilesDir.FullName, hostPacket, hostFileName);
            Rule changedRule = new Rule();
            var newChangedRulesFilePath = FileHandler.CreateNewRulesFile(_ruleFileDir.FullName, changedRule.GetRule(), rulesFileName);

            _console = new FirewallConsoleData(newRulesFilePath + " " + newHostFilePath);

            _console = new FirewallConsoleData(newChangedRulesFilePath + " " + newHostFilePath);

            Assert.That(_console.GetConsoleOutput()[0].Action, Is.EqualTo(expectedResult));
        }

        /// <summary>
        /// FirewallTests_ReloadDataForHostsFile_ReturnedNewData
        /// Add new host file and get 1 output from Firewall
        /// Add another host file and get 2 oututs from Firewall
        /// </summary>
        /// <param name="user"></param>
        /// <param name="destination"></param>
        /// <param name="ruleAction"></param>
        /// <param name="expectedResultOne"></param>
        /// <param name="expectedResultTwo"></param>
        [Test]
        [TestCase("TestUser@domain.com", "192.168.0.1", "allow", 1, 2)]
        public void FirewallTests_ReloadDataForHostsFile_ReturnedNewData(string user, string destination, string ruleAction, int expectedResultOne, int expectedResultTwo )
        {
            Rule rule = new Rule(user, destination, ruleAction);
            string rulesFileName = "Rules.csv";
            string hostPacket = new Host().GetHostPacket();
            string hostPacket2 = new Host(user = "ReloadHostUser1@domain.com", destination = "192.168.100.100").GetHostPacket();
            string hostFileName = "192.168.1.10.csv";
            string hostFileName2 = "192.168.1.11.csv";

            var newRulesFilePath = FileHandler.CreateNewRulesFile(_ruleFileDir.FullName, rule.GetRule(), rulesFileName);
            var newHostFilePath = FileHandler.CreateNewHostsFile(_hostsFilesDir.FullName, hostPacket, hostFileName);

            _console = new FirewallConsoleData(newRulesFilePath + " " + newHostFilePath);
            Assert.That(_console.GetConsoleOutput().Count, Is.EqualTo(expectedResultOne));

            var anotherHostsFile = FileHandler.CreateNewHostsFile(_hostsFilesDir.FullName, hostPacket2, hostFileName2);

            _console = new FirewallConsoleData(newRulesFilePath + " " + newHostFilePath);

            Assert.That(_console.GetConsoleOutput().Count, Is.EqualTo(expectedResultTwo));
        }
    }
}
