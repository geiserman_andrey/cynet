﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleReader;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using System.Configuration;

namespace UnitTests
{
    [TestFixture]
    public class FirewallTestsForRulesFile
    {
        private static DirectoryInfo _directoryWithSpaces = null;
        private static DirectoryInfo _hostsFilesDir = null;
        private static DirectoryInfo _directoryWithoutSpaces = null;
        private FirewallConsoleData _console;

        [SetUp]
        public void TestFixtureSetUp()
        {
            // Set current folder to testing folder
            string assemblyCodeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            string dirName = Path.GetDirectoryName(assemblyCodeBase);

            if (dirName.StartsWith("file:\\"))
                dirName = dirName.Substring(6);

            Environment.CurrentDirectory = dirName;

            _directoryWithSpaces = System.IO.Directory.CreateDirectory(dirName + ConfigurationManager.AppSettings["DirNameIncludeSpaces"]);
            _directoryWithoutSpaces = System.IO.Directory.CreateDirectory(dirName + ConfigurationManager.AppSettings["DirNameIncludeNoSpaces"]);
            //_ruleFileDir = System.IO.Directory.CreateDirectory(dirName + ConfigurationManager.AppSettings["DirNameForRulesFile"]);
            _hostsFilesDir = System.IO.Directory.CreateDirectory(dirName + ConfigurationManager.AppSettings["DirNameForHostsFile"]);

        }

        [TearDown]
        public void TestMixtureTearDown()
        {
            foreach (FileInfo file in _directoryWithSpaces.GetFiles())
            {
                file.Delete();
            }
            foreach (FileInfo file in _directoryWithoutSpaces.GetFiles())
            {
                file.Delete();
            }
            foreach (FileInfo file in _hostsFilesDir.GetFiles())
            {
                file.Delete();
            }
        }

        /// <summary>
        /// FirewallTests_PathWithSpacesUsed_ReturnCorrespondingExitCode
        /// Must return 0 exit code whether path inlcudes spaces or not
        /// </summary>
        [Test]
        public void FirewallTests_PathWithSpacesUsed_ReturnCorrespondingExitCode()
        {
            string rule = new Rule().GetRule();
            string rulesFileName = "Rules_" + DateTime.UtcNow.Ticks + ".csv";
            string hostPacket = new Host().GetHostPacket();
            string hostFileName = "192.168.1.10.csv";
            var newRulesFilePath = FileHandler.CreateNewRulesFile(_directoryWithSpaces.FullName, rule, rulesFileName);
            var newHostFilePath = FileHandler.CreateNewHostsFile(_hostsFilesDir.FullName, hostPacket, hostFileName);
            _console = new FirewallConsoleData(newRulesFilePath + " " + newHostFilePath);

            Assert.AreEqual(0, _console.ReturnExitCode());
        }

        /// <summary>
        /// FirewallTests_PathWithoutSpacesUsed_ReturnCorrespondingExitCode
        /// Must return 0 exit code whether path inlcudes spaces or not
        /// </summary>
        [Test]
        public void FirewallTests_PathWithoutSpacesUsed_ReturnCorrespondingExitCode()
        {
            string rule = new Rule().GetRule();
            string rulesFileName = "Rules_" + DateTime.UtcNow.Ticks + ".csv";
            string hostPacket = new Host().GetHostPacket();
            string hostFileName = "192.168.1.10.csv";
            var newRulesFilePath = FileHandler.CreateNewRulesFile(_directoryWithoutSpaces.FullName, rule, rulesFileName);
            var newHostFilePath = FileHandler.CreateNewHostsFile(_hostsFilesDir.FullName, hostPacket, hostFileName);
            _console = new FirewallConsoleData(newRulesFilePath + " " + newHostFilePath);

            Assert.AreEqual(0, _console.ReturnExitCode());
        }


        /// <summary>
        /// Firewalltests_InvalidUserNameProvided_ReturnExitCodeForInvalidUsername
        /// must return exit code in case when user name provided not in desired format
        /// "0" code should be replaced for corresponding return code in such case
        /// "2" is used as temp exit code for invalid user input
        /// </summary>
        [Test]
        [TestCase("OiInternet@domain", "177.41.11.124", "deny", 2)]
        [TestCase("OiInternet@domaincom", "177.41.11.124", "deny", 2)]
        [TestCase("OiInternet@", "177.41.12.124", "deny", 2)]
        [TestCase("@domain.com", "177.41.12.124", "deny", 2)]
        public void FirewallTests_InvalidUserNameProvided_ReturnExitCodeForInvalidUserName(string user, string destination, string action, int expectedExitCode)
        {
            string rule = new Rule(user, destination, action).GetRule();
            string rulesFileName = "Rules_WrongUser_" + DateTime.UtcNow.Ticks + ".csv";
            string hostPacket = new Host().GetHostPacket();
            string hostFileName = "192.168.1.10.csv";
            var newRulesFilePath = FileHandler.CreateNewRulesFile(_directoryWithoutSpaces.FullName, rule, rulesFileName);
            var newHostFilePath = FileHandler.CreateNewHostsFile(_hostsFilesDir.FullName, hostPacket, hostFileName);
            _console = new FirewallConsoleData(newRulesFilePath + " " + newHostFilePath);

            Assert.AreEqual(expectedExitCode, _console.ReturnExitCode());
        }

        /// <summary>
        /// FirewallTests_PassRulesFileWithInvalidDestination_ReturnСorrespondingExitCode
        /// must return exit code in case when wrong destination provided
        /// "0" code should be replaced for proper return code in such case
        /// </summary>
        [Test]
        [TestCase("OiInternet@domain.com", "177.41.124", "deny", 2)]
        [TestCase("OiInternet@domain.com", "177.300.41.124", "deny", 2)]
        [TestCase("OiInternet@domain.com", "177.41..124", "deny", 2)]
        [TestCase("OiInternet@domain.com", "....", "deny", 2)]
        [TestCase("OiInternet@domain.com", " ", "deny", 2)]
        [TestCase("OiInternet@domain.com", "177.41.124", "deny", 2)]
        public void FirewallTests_InvalidDestinationProvided_ReturnСorrespondingExitCode(string user, string destination, string action, int expectedExitCode)
        {
            string rule = new Rule(user, destination, action).GetRule();
            string rulesFileName = "Rules_InvalidDestination_" + DateTime.UtcNow.Ticks + ".csv";
            string hostPacket = new Host().GetHostPacket();
            string hostFileName = "192.168.1.10.csv";
            var newRulesFilePath = FileHandler.CreateNewRulesFile(_directoryWithoutSpaces.FullName, rule, rulesFileName);
            var newHostFilePath = FileHandler.CreateNewHostsFile(_hostsFilesDir.FullName, hostPacket, hostFileName);
            _console = new FirewallConsoleData(newRulesFilePath + " " + newHostFilePath);

            Assert.AreEqual(expectedExitCode, _console.ReturnExitCode());
        }
    }
}
